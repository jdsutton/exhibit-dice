import('/src/JSUtils/math/Random.js');
import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {


    /**
     * @public
     */
    static init() {
        super.init();

        this._labelDice();
    }

    /**
     * @private
     */
    static _labelDice() {
        const N = 6;
        const labels = [
            Random.chooseN(this._AREAS_OF_SCIENCE, N),
            Random.chooseN(this._EXHIBIT_PROPERTIES, N),
            Random.chooseN(this._EXHIBIT_TYPES, N),
        ];

        for (let i in ID.ALL_DICE) {
            const dice = ID.ALL_DICE[i];

            Dom.setContents(dice, '');

            for (let j in labels[i]) {
                const label = labels[i][j];
                const c = ID.ALL_SIDES[j];
                
                Dom.appendTo(dice, `<div class="${c} FlexColumn FlexVerticalAlign">${label}</div>`);
            }
        }
    }

    /**
     * @private
     * Based on this project by SteveJRobertson:
     * https://codepen.io/SteveJRobertson/pen/zxEwrK
     */
    static _rollDice(dice) {
        dice = Dom.getById(dice);

        const min = 1;
        const max = 24;

        const xRand = Math.floor(Random.range(max, min)) * 90;
        // var yRand = Math.floor(Random.range(max, min)) * 90;
        const yRand = 0;

        dice.style.webkitTransform = 'rotateX('+xRand+'deg) rotateY('+yRand+'deg)';
        dice.style.transform = 'rotateX('+xRand+'deg) rotateY('+yRand+'deg)';
    }

    /**
     * @public
     */
    static onRoll() {
        this._labelDice();

        for (let dice of ID.ALL_DICE) {
            this._rollDice(dice);
        }
    }
}
    
HomepageViewDriver._AREAS_OF_SCIENCE = [
    'Logic',
    'Data science',
    'Mathematics',
    'Statistics',
    'Systems theory',
    'Decision theory',
    'Computer Science',
    'Physics',
    'Chemistry',
    'Earth science',
    'Geology',
    'Oceanography',
    'Meteorology',
    'Astronomy',
    'Biology',
    'Biochemistry',
    'Microbiology',
    'Botany',
    'Zoology',
    'Ecology',
];

HomepageViewDriver._EXHIBIT_PROPERTIES = [
    'Immersive',
    'Sensory',
    'Thought-provoking',
    'Historical',
    'Cultural',
    'Aesthetic',
    'Spatial',
    'Social',
    'Chronologicical',
    'Inclusive',
    'Dynamic',
    'Interdisciplinary',
];

HomepageViewDriver._EXHIBIT_TYPES = [
    'Hands-On',
    'Sculpture',
    'Ride',
    'Traditional artwork',
    'Digitial art',
    'Photography',
    'Artifacts',
    'Installation',
    'Virtual reality',
    'Audiovisual',
    'Pop-up',
    'Performance art',
    'Diorama',
    'Educational display',
    'Technology showcase',
    'Architecture',
    'Fashion show',
    'Living exhibit',

];

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    DICE1: 'dice1',
    DICE2: 'dice2',
    DICE3: 'dice3',
    ALL_DICE: ['dice1', 'dice2', 'dice3'],
    ALL_SIDES: ['front', 'right', 'left', 'back', 'top', 'bottom']
};

const CLASS = {

};
