#!/bin/bash
# Fast build for development.

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SLURP=$SCRIPTPATH/../src/JSUtils/Slurp/slurp/build
SRC=$1
DIST=$2
TMP=_DIST_TMP

rm -rf $TMP
mkdir -p $TMP

echo "Run Slurp pipeline."
$SLURP/slurpUp.py $SRC $DIST \
| $SLURP/ignore.py node_modules test tests scripts \
| $SLURP/spit.py --fileExtensions=png,jpg,jpeg,svg \
| $SLURP/fileLinker.py --fileExtensions=js,html,css \
| $SLURP/spit.py

rm -rf $TMP
