# Exhibit Dice
Inspiration for exhibits innovation!
By John the Exhibits Guy!

[Try the dice here!](https://exhibit-dice-jdsutton-536d0b498489343ddeaa7a134c6fd26faf41af8e1.gitlab.io/)

## Getting Started

### Installation
```
git clone https://gitlab.com/jdsutton/exhibit-dice.git
cd exhibit-dice

sudo apt update
sudo apt install virtualenv build-essential

# Make sure python3.6 is installed on your system:
python3 --version

virtualenv -p python3.6 env
. env/bin/activate

make install
```

### Run the webserver
```
# Make sure you have done . env/bin/activate first

make
```

Now you may open the page at http://0.0.0.0:50000